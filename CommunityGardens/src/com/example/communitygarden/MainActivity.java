package com.example.communitygarden;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import com.esri.android.map.Layer;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISDynamicMapServiceLayer;
import com.esri.android.map.ags.ArcGISFeatureLayer;
import com.esri.android.map.ags.ArcGISLayerInfo;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.esri.android.map.event.OnSingleTapListener;
import com.esri.android.map.popup.PopupContainer;
import com.esri.core.geometry.Envelope;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.FeatureSet;
import com.esri.core.map.Graphic;
import com.esri.core.map.popup.PopupInfo;
import com.esri.android.map.popup.Popup;
import com.esri.android.map.popup.PopupContainer;
import com.esri.core.tasks.ags.query.Query;
import com.esri.core.tasks.ags.query.QueryTask;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends Activity {
	Button basicButton, zoningButton, mapButton, projectButton,homeButton,benefitButton, descriptButton, flaglerButton,
	davieButton, hollywoodButton; 
	
	final Context context=this;
	MapView mMapView;
	ArcGISTiledMapServiceLayer tileLayer;
	ArcGISFeatureLayer gardenFlags;
	/////
	 PopupContainer popupContainer;
	  PopupDialog popupDialog;
	  ProgressDialog progressDialog;
	  AtomicInteger count;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		welcome();		
	}
	
	private void zoning()
	{
		setContentView(R.layout.zoning);
		homeButton=(Button) findViewById(R.id.home);
		mMapView = (MapView) findViewById(R.id.mapZ);
		
		/**
		 * Responds to user touch on map
		 */
		 mMapView.setOnSingleTapListener(new OnSingleTapListener() {
		      private static final long serialVersionUID = 1L;

		      public void onSingleTap(float x, float y) {    	
		        if (mMapView.isLoaded()) {
		        	// Instantiate a PopupContainer
		        	popupContainer = new PopupContainer(mMapView);
		        	int id = popupContainer.hashCode();
		        	popupDialog = null;
		        	// Display spinner.
		        	if (progressDialog == null || !progressDialog.isShowing())
		        		progressDialog = ProgressDialog.show(mMapView.getContext(), "", "Querying...");

		        	/**
		        	 * Loop through each layer in the webmap 
		        	 */
		        	int tolerance = 20;
							Envelope env = new Envelope(mMapView.toMapPoint(x, y), 20 * mMapView.getResolution(), 20 * mMapView.getResolution());
		        	Layer[] layers = mMapView.getLayers();
		        	count = new AtomicInteger();
		        	for (Layer layer : layers) {
		        		// If the layer has not been initialized or is invisible, do nothing.
		      			if (!layer.isInitialized() || !layer.isVisible())
		      				continue;
		      			
		      			if (layer instanceof ArcGISFeatureLayer) { 
		      				// Query feature layer and display popups
		      				ArcGISFeatureLayer featureLayer = (ArcGISFeatureLayer) layer;          				
		      				if (featureLayer.getPopupInfo() != null) {
		      					// Query feature layer which is associated with a popup definition.
		      					count.incrementAndGet();
		      					new RunQueryFeatureLayerTask(x, y, tolerance, id).execute(featureLayer);
		      				}
		      			}
		      			else if (layer instanceof ArcGISDynamicMapServiceLayer) { 
		      				// Query dynamic map service layer and display popups.
		      				ArcGISDynamicMapServiceLayer dynamicLayer = (ArcGISDynamicMapServiceLayer) layer;
		      				// Retrieve layer info for each sub-layer of the dynamic map service layer.
		      				ArcGISLayerInfo[] layerinfos = dynamicLayer.getAllLayers();
		      				if (layerinfos == null)
		      					continue;
		      				
		      				// Loop through each sub-layer
		      				for (ArcGISLayerInfo layerInfo : layerinfos) {
		      					// Obtain PopupInfo for sub-layer.
		      					PopupInfo popupInfo = dynamicLayer.getPopupInfo(layerInfo.getId());
		      					// Skip sub-layer which is without a popup definition.
		      					if (popupInfo == null) {
		      						continue;
		      					}
		      					// Check if a sub-layer is visible.
		      					ArcGISLayerInfo info = layerInfo;
		      					while ( info != null && info.isVisible() ) {
		      						info = info.getParentLayer();
		      					}
		      					// Skip invisible sub-layer
		      					if ( info != null && ! info.isVisible() ) {
		      						continue;
		      					};

		      					// Check if the sub-layer is within the scale range
		      					double maxScale = (layerInfo.getMaxScale() != 0) ? layerInfo.getMaxScale():popupInfo.getMaxScale();
		      					double minScale = (layerInfo.getMinScale() != 0) ? layerInfo.getMinScale():popupInfo.getMinScale();

		      					if ((maxScale == 0 || mMapView.getScale() > maxScale) && (minScale == 0 || mMapView.getScale() < minScale)) {
		      						// Query sub-layer which is associated with a popup definition and is visible and in scale range.
		      						count.incrementAndGet();
		      						new RunQueryDynamicLayerTask(env, layer, layerInfo.getId(), dynamicLayer.getSpatialReference(), id).execute(dynamicLayer.getUrl() + "/" + layerInfo.getId());
		      					}
		      				}
		      			}      			
		        	}
		        }
		      }
		    });
		 
		 /**
		  * Responds to home button touch
		  */
		homeButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setContentView(R.layout.activity_main);
				welcome();
			}
		});		
	}
	

	/**
	 * Sets up the basic info layout
	 */
	private void basicInfo()
	{
		setContentView(R.layout.basicinfo);
		homeButton=(Button) findViewById(R.id.homeB);
		homeButton.setVisibility(View.VISIBLE);
		homeButton.setBackgroundColor(Color.TRANSPARENT);
		
		benefitButton=(Button) findViewById(R.id.benefitBtn);
		benefitButton.setVisibility(View.VISIBLE);
		benefitButton.setBackgroundColor(Color.TRANSPARENT);
		
		
		descriptButton=(Button) findViewById(R.id.descriptbtn);
		descriptButton.setVisibility(View.VISIBLE);
		descriptButton.setBackgroundColor(Color.TRANSPARENT);
		
		
		
		/**
		 * Responds to home button touch
		 */
		homeButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setContentView(R.layout.activity_main);
				welcome();
			}
		});	
		
		/**
		 * Responds to benefit button touch
		 */
		benefitButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
				alertDialogBuilder.setTitle("Community Garden Benefits");
				alertDialogBuilder.setMessage(R.string.benefitString).setCancelable(true);
				alertDialogBuilder.setPositiveButton("Close",null);
				AlertDialog alertDialog=alertDialogBuilder.create();
				alertDialog.show();				
			}
		});
		
		/**
		 * Responds to description button touch
		 */
		descriptButton.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
				alertDialogBuilder.setTitle("Community Garden Description");
				alertDialogBuilder.setMessage(R.string.descriptString).setCancelable(true);
				alertDialogBuilder.setPositiveButton("Close",null);
				AlertDialog alertDialog=alertDialogBuilder.create();
				alertDialog.show();
			}
		});		
	}
	
	/**
	 * sets up the may layout
	 */
	private void mapping()
	{
		
		setContentView(R.layout.mapping);
		
		
		homeButton=(Button) findViewById(R.id.homeM);
		mMapView = (MapView) findViewById(R.id.map2);
		
		
		 mMapView.setOnSingleTapListener(new OnSingleTapListener() {
		      private static final long serialVersionUID = 1L;

		      public void onSingleTap(float x, float y) {    	
		        if (mMapView.isLoaded()) {
		        	// Instantiate a PopupContainer
		        	popupContainer = new PopupContainer(mMapView);
		        	int id = popupContainer.hashCode();
		        	popupDialog = null;
		        	// Display spinner.
		        	if (progressDialog == null || !progressDialog.isShowing())
		        		progressDialog = ProgressDialog.show(mMapView.getContext(), "", "Querying...");

		        	// Loop through each layer in the webmap
		        	int tolerance = 20;
							Envelope env = new Envelope(mMapView.toMapPoint(x, y), 20 * mMapView.getResolution(), 20 * mMapView.getResolution());
		        	Layer[] layers = mMapView.getLayers();
		        	count = new AtomicInteger();
		        	for (Layer layer : layers) {
		        		// If the layer has not been initialized or is invisible, do nothing.
		      			if (!layer.isInitialized() || !layer.isVisible())
		      				continue;
		      			
		      			if (layer instanceof ArcGISFeatureLayer) { 
		      				// Query feature layer and display popups
		      				ArcGISFeatureLayer featureLayer = (ArcGISFeatureLayer) layer;          				
		      				if (featureLayer.getPopupInfo() != null) {
		      					// Query feature layer which is associated with a popup definition.
		      					count.incrementAndGet();
		      					new RunQueryFeatureLayerTask(x, y, tolerance, id).execute(featureLayer);
		      				}
		      			}
		      			else if (layer instanceof ArcGISDynamicMapServiceLayer) { 
		      				// Query dynamic map service layer and display popups.
		      				ArcGISDynamicMapServiceLayer dynamicLayer = (ArcGISDynamicMapServiceLayer) layer;
		      				// Retrieve layer info for each sub-layer of the dynamic map service layer.
		      				ArcGISLayerInfo[] layerinfos = dynamicLayer.getAllLayers();
		      				if (layerinfos == null)
		      					continue;
		      				
		      				// Loop through each sub-layer
		      				for (ArcGISLayerInfo layerInfo : layerinfos) {
		      					// Obtain PopupInfo for sub-layer.
		      					PopupInfo popupInfo = dynamicLayer.getPopupInfo(layerInfo.getId());
		      					// Skip sub-layer which is without a popup definition.
		      					if (popupInfo == null) {
		      						continue;
		      					}
		      					// Check if a sub-layer is visible.
		      					ArcGISLayerInfo info = layerInfo;
		      					while ( info != null && info.isVisible() ) {
		      						info = info.getParentLayer();
		      					}
		      					// Skip invisible sub-layer
		      					if ( info != null && ! info.isVisible() ) {
		      						continue;
		      					};

		      					// Check if the sub-layer is within the scale range
		      					double maxScale = (layerInfo.getMaxScale() != 0) ? layerInfo.getMaxScale():popupInfo.getMaxScale();
		      					double minScale = (layerInfo.getMinScale() != 0) ? layerInfo.getMinScale():popupInfo.getMinScale();

		      					if ((maxScale == 0 || mMapView.getScale() > maxScale) && (minScale == 0 || mMapView.getScale() < minScale)) {
		      						// Query sub-layer which is associated with a popup definition and is visible and in scale range.
		      						count.incrementAndGet();
		      						new RunQueryDynamicLayerTask(env, layer, layerInfo.getId(), dynamicLayer.getSpatialReference(), id).execute(dynamicLayer.getUrl() + "/" + layerInfo.getId());
		      					}
		      				}
		      			}      			
		        	}
		        }
		      }
		    });
		//// 
		homeButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Retrieve the map and initial extent from XML layout
			
	
				setContentView(R.layout.activity_main);
				welcome();
			}
		});	
	}
	 
	/**
	 * Create popup view
	 * @param graphics
	 * @param id
	 */
	private void createPopupViews(Graphic[] graphics, final int id) {
		if ( id != popupContainer.hashCode() ) {
			if (progressDialog != null && progressDialog.isShowing() && count.intValue() == 0) 
				progressDialog.dismiss();

			return;
		}

		if (popupDialog == null) {
			if (progressDialog != null && progressDialog.isShowing()) 
				progressDialog.dismiss();
			
			// Create a dialog for the popups and display it.
			/**
			 * Create a dialog for the popups and display it.
			 */
			popupDialog = new PopupDialog(mMapView.getContext(), popupContainer);
			
			popupDialog.show();
		}
  }
  

	/**
	 * 
	 * @author 
	 * Query feature layer by hit test.
	 *
	 */
  private class RunQueryFeatureLayerTask extends AsyncTask<ArcGISFeatureLayer, Void, Graphic[]> {

		private int tolerance;
		private float x;
		private float y;
		private ArcGISFeatureLayer featureLayer;
		private int id;

		public RunQueryFeatureLayerTask(float x, float y, int tolerance, int id) {
			super();
			this.x = x;
			this.y = y;
			this.tolerance = tolerance;
			this.id = id;
		}

		@Override
		protected Graphic[] doInBackground(ArcGISFeatureLayer... params) {
			for (ArcGISFeatureLayer featureLayer : params) {
				this.featureLayer = featureLayer;
				// Retrieve graphic ids near the point.
				int[] ids = featureLayer.getGraphicIDs(x, y, tolerance);
				if (ids != null && ids.length > 0) {
					ArrayList<Graphic> graphics = new ArrayList<Graphic>();
					for (int id : ids) {
						// Obtain graphic based on the id.
						Graphic g = featureLayer.getGraphic(id);
						if (g == null)
							continue;
						graphics.add(g);
					}
					// Return an array of graphics near the point.
					return graphics.toArray(new Graphic[0]);
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Graphic[] graphics) {
			count.decrementAndGet();
			if (graphics == null || graphics.length == 0) {
				if (progressDialog != null && progressDialog.isShowing() && count.intValue() == 0) 
					progressDialog.dismiss();
				
				return;
			}
			// Check if the requested PopupContainer id is the same as the current PopupContainer.
			// Otherwise, abandon the obsoleted query result.
			if ( id != popupContainer.hashCode() ) {
				if (progressDialog != null && progressDialog.isShowing() && count.intValue() == 0) 
					progressDialog.dismiss();
				
				return;
			}

			for (Graphic gr : graphics) {
				Popup popup = featureLayer.createPopup(mMapView, 0, gr);
				popupContainer.addPopup(popup);
			}
			createPopupViews(graphics, id);
		}

	}
  
  // Query dynamic map service layer by QueryTask
  /**
   * 
   * @author 
   * Query dynamic map service layer by QueryTaks
   *
   */
  private class RunQueryDynamicLayerTask extends AsyncTask<String, Void, FeatureSet> {
		private Envelope env;
		private SpatialReference sr;
		private int id;
		private Layer layer;
		private int subLayerId;

		public RunQueryDynamicLayerTask(Envelope env, Layer layer, int subLayerId, SpatialReference sr, int id) {
			super();
			this.env = env;
			this.sr = sr;
			this.id = id;
			this.layer = layer;
			this.subLayerId = subLayerId;
		}

		@Override
		protected FeatureSet doInBackground(String... urls) {
			for (String url : urls) {
				// Retrieve graphics within the envelope.
				Query query = new Query();
				query.setInSpatialReference(sr);
				query.setOutSpatialReference(sr);
				query.setGeometry(env);
				query.setMaxFeatures(10);
				query.setOutFields(new String[] { "*" });

				QueryTask queryTask = new QueryTask(url);
				try {
					FeatureSet results = queryTask.execute(query);
					return results;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(final FeatureSet result) {
			count.decrementAndGet();
			if (result == null) {
				if (progressDialog != null && progressDialog.isShowing() && count.intValue() == 0) 
					progressDialog.dismiss();

				return;
			}
			Graphic[] graphics = result.getGraphics();
			if (graphics == null || graphics.length == 0) {
				if (progressDialog != null && progressDialog.isShowing() && count.intValue() == 0) 
					progressDialog.dismiss();

				return;
			}
			
			/**
			 * Check if the requested PopupContainer id is the same as the current PopupContainer 
			 * Otherwise, abandon the obsoleted query result
			 */
			if (id != popupContainer.hashCode()) {
		
				/**
				 * Dismiss spinner
				 */
				if (progressDialog != null && progressDialog.isShowing() && count.intValue() == 0) 
					progressDialog.dismiss();

				return;
			}
			PopupInfo popupInfo = layer.getPopupInfo(subLayerId);
			if (popupInfo == null) {
				// Dismiss spinner
				if (progressDialog != null && progressDialog.isShowing() && count.intValue() == 0)  
					progressDialog.dismiss();

				return;
			}
			
			for (Graphic gr : graphics) {
				Popup popup = layer.createPopup(mMapView, subLayerId, gr);
				popupContainer.addPopup(popup);
			}
			createPopupViews(graphics, id);
			
		}
	}
  
  
  /**
   * 
   * @author
   * A customize full screen dialog
   *
   */
  private class PopupDialog extends Dialog {
	  private PopupContainer popupContainer;
	  
	  public PopupDialog(Context context, PopupContainer popupContainer) {
		  super(context, android.R.style.Theme);
		  this.popupContainer = popupContainer;
	  }
	  
	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
		  super.onCreate(savedInstanceState);
		  LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			LinearLayout layout = new LinearLayout(getContext());
			layout.addView(popupContainer.getPopupContainerView(), android.widget.LinearLayout.LayoutParams.WRAP_CONTENT, android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
			setContentView(layout, params);
			
		}
	  
  }
  
  
  /**
   * set up the project layout
   */
	private void project()
	{
		setContentView(R.layout.projects);
		homeButton=(Button) findViewById(R.id.homeP);
		homeButton.setVisibility(View.VISIBLE);
		homeButton.setBackgroundColor(Color.TRANSPARENT);
		
		flaglerButton= (Button) findViewById(R.id.flagerBtn);
		flaglerButton.setVisibility(View.VISIBLE);
		flaglerButton.setBackgroundColor(Color.TRANSPARENT);
		
		davieButton= (Button) findViewById(R.id.davieBtn);
		davieButton.setVisibility(View.VISIBLE);
		davieButton.setBackgroundColor(Color.TRANSPARENT);
		
		hollywoodButton= (Button) findViewById(R.id.hollywoodBtn);
		hollywoodButton.setVisibility(View.VISIBLE);
		hollywoodButton.setBackgroundColor(Color.TRANSPARENT);
		
		
		//
		homeButton.setOnClickListener(new View.OnClickListener() {
		
		
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setContentView(R.layout.activity_main);
				welcome();
			}
		});	
		
		flaglerButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final TextView message = new TextView(context);
				final SpannableString s = new SpannableString(context.getText(R.string.flaglergardenString));
				Linkify.addLinks(s, Linkify.WEB_URLS);
				AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
				message.setText(s);
				message.setMovementMethod(LinkMovementMethod.getInstance());
				alertDialogBuilder.setTitle("Flagler Garden Fort Lauderdale");
				//alertDialogBuilder.setMessage(R.string.flaglergardenString).setCancelable(true);
				alertDialogBuilder.setCancelable(true).setView(message);
				alertDialogBuilder.setPositiveButton("Close",null);
				AlertDialog alertDialog=alertDialogBuilder.create();				
				alertDialog.show();	
				
				
			}
		});
		
		davieButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final TextView message = new TextView(context);
				final SpannableString s = new SpannableString(context.getText(R.string.daviegardenString));
				Linkify.addLinks(s, Linkify.WEB_URLS);
				AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
				message.setText(s);
				message.setMovementMethod(LinkMovementMethod.getInstance());
				alertDialogBuilder.setTitle("Town of Davie Community Garden");
				//alertDialogBuilder.setMessage(R.string.flaglergardenString).setCancelable(true);
				alertDialogBuilder.setCancelable(true).setView(message);
				alertDialogBuilder.setPositiveButton("Close",null);
				AlertDialog alertDialog=alertDialogBuilder.create();				
				alertDialog.show();	
				
			}
		});
		
		
		hollywoodButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final TextView message = new TextView(context);
				final SpannableString s = new SpannableString(context.getText(R.string.hollywoodgardenString));
				Linkify.addLinks(s, Linkify.WEB_URLS);
				AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
				message.setText(s);
				message.setMovementMethod(LinkMovementMethod.getInstance());
				alertDialogBuilder.setTitle("Need to feed Hollywood Garden");
				//alertDialogBuilder.setMessage(R.string.flaglergardenString).setCancelable(true);
				alertDialogBuilder.setCancelable(true).setView(message);
				alertDialogBuilder.setPositiveButton("Close",null);
				AlertDialog alertDialog=alertDialogBuilder.create();				
				alertDialog.show();	
				
			}
		});
		
	}

	/**
	 * set up the welcome layout
	 */
	private void welcome()
	{
	basicButton=(Button) findViewById(R.id.basic_info);
	basicButton.setVisibility(View.VISIBLE);
	basicButton.setBackgroundColor(Color.TRANSPARENT);
	
	zoningButton=(Button) findViewById(R.id.zoning);
	zoningButton.setVisibility(View.VISIBLE);
	zoningButton.setBackgroundColor(Color.TRANSPARENT);
	
	mapButton=(Button) findViewById(R.id.mapping);
	mapButton.setVisibility(View.VISIBLE);
	mapButton.setBackgroundColor(Color.TRANSPARENT);
	
	projectButton=(Button) findViewById(R.id.major_projects);
	projectButton.setVisibility(View.VISIBLE);
	projectButton.setBackgroundColor(Color.TRANSPARENT);

	
	basicButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			basicInfo();			
		}
	});
	
	zoningButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			zoning();			
		}
	});
	
	mapButton.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		mapping();		
	}
});
	
	projectButton.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		project();	
	}
});
	
}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}


